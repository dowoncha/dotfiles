#!/bin/sh

install_pip() {
  # Check if pip is installed
  sudo apt install python3-pip
}

install_src() {
  git clone https://github.com/ansible/ansible.git --recursive
  cd ./ansible
  # -q - Supress warnings/errors
  source ./hacking/env-setup -q
  install_pip
  sudo pip install -r ./requirements.txt
}

install_ubuntu() {
	sudo apt update
	sudo apt install software-properties-common
	sudo apt-add-repository --yes --update ppa:ansible/ansible
	sudo apt-get install ansible
}

install_src

# Test
ansible all -m ping --ask-pass
