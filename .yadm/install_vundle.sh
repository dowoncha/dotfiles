#!/bin/sh

# Is vundle installed?

VUNDLE_DIRECTORY=~/.vim/bundle/Vundle.vim

if [ -d "$VUNDLE_DIRECTORY"]; then
  git clone https://github.com/Vundlevim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  echo "Cloned vundle source"
else
  echo "Vundle found in $VUNDLE_DIRECTORY"
fi

echo "Installing Vim Plugins"
vim +PluginInstall +qall

# Compile YouCompleteMe
echo "Compiling Youcompleteme"
cd ~/.vim/bundle/youcompleteme
python3 ./install.py --all
