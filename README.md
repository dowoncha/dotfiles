# My Dotfiles

Intended for Ubuntu Base

Uses Bash shell

## Tools
* rxvt-unicode - terminal emulator
* Vim
* Vundle - Vim package manager
* Terraform - Infrastructure as Code
* Ansible - app deployment, config management, continuous delivery
* Digital Ocean Ctl
* Ranger - file browser
* Neofetch - system info display
* Docker
* Tmux
* Timeshift - System backup/restore
* htop - process manager
* vs codium - editor

Font - Fira Sans

## Languages
* Rust - rustup, cargo, rust
* Python3 - pip3, virtualenv
* Node - nvm, npm, node, npx, create-react-app
* Typescript - tsc
* C/C++ - clang
* Lisp - racket

# Setup process
